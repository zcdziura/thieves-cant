/*
 * Copyright (C) 2018 Z. Charles Dziura
 *
 * This program is distributed under the terms of the GNU General Public
 * License. See the `COPYING` file for full terms.
 */

use std::iter::repeat;

use regex::{Captures, Regex};

use crate::deck::Deck;

pub fn generate_key_stream(deck: &mut Deck) -> u8 {
    let output: u8;

    loop {
        deck.move_joker_a();
        deck.move_joker_b();
        deck.triple_cut();
        deck.count_cut();

        match deck.get_output_value() {
            Ok(c) => {
                output = c;
                break;
            }
            Err(_) => continue,
        }
    }

    output
}

pub fn encode_special_characters(string: &str) -> String {
    let mut encoded_string = String::new();
    for c in string.chars() {
        if c.is_ascii_alphabetic() {
            encoded_string.push(c.to_ascii_uppercase());
        } else {
            encoded_string.push_str(match c {
                ' ' => "XZA",
                '.' => "XZB",
                ',' => "XZC",
                '\'' => "XZD",
                '?' => "XZE",
                '!' => "XZF",
                _ => panic!("Thieves' Cant Error: Invalid character {}", c)
            });
        }
    }

    encoded_string
}

pub fn decode_special_characters(string: &str) -> String {
    lazy_static! {
        static ref RE: Regex = Regex::new("XZ(?P<code>[A-F])").unwrap();
    }

    RE.replace_all(string, |caps: &Captures| {
        match caps.name("code").unwrap().as_str() {
            "A" => " ",
            "B" => ".",
            "C" => ",",
            "D" => "'",
            "E" => "?",
            "F" => "!",
            _ => unreachable!(
                "Something went wrong when parsing the string with regular expressions"
            ),
        }
    }).into()
}

pub fn pad_string(string: &str) -> String {
    let mut padded_string = String::from(string);
    if string.len() > 0 && string.len() % 5 != 0 {
        let z_padding = repeat('Z')
            .take(5 - (padded_string.len() % 5))
            .collect::<String>();
        padded_string.push_str(&z_padding);
    }

    padded_string
}

pub fn encrypt_plaintext(string: &str, deck: Deck) -> String {
    let text = encode_special_characters(string);
    let mut deck = deck;
    let mut output = String::new();

    for c in text.chars() {
        if c != ' ' {
            let key = generate_key_stream(&mut deck);
            let mut ciphertext = key + (c as u8 - 64);

            while ciphertext > 26 {
                ciphertext -= 26
            }

            output.push(char::from(ciphertext + 64));
        }
    }

    output
}

pub fn decrypt_ciphertext(string: &str, deck: Deck) -> String {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"\s").unwrap();
    }

    let mut deck = deck;
    let mut output = String::new();
    let text = RE.replace_all(string, "");

    for c in text.chars() {
        let key = generate_key_stream(&mut deck);
        let mut plaintext = (c as i8 - 64) - key as i8;

        while plaintext < 1 {
            plaintext += 26;
        }

        output.push(char::from(plaintext as u8 + 64))
    }

    output
}

pub fn format_string(string: &str) -> String {
    let mut formatted_string = String::new();
    let mut chars_pushed = 0;

    for (idx, c) in string.chars().enumerate() {
        if c != ' ' {
            formatted_string.push(c);
            chars_pushed += 1;

            if chars_pushed > 0 && idx != string.len() - 1 && chars_pushed % 5 == 0 {
                formatted_string.push(' ');
            }
        }
    }

    formatted_string
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::deck::Deck;

    #[test]
    fn test_encode_good_string() {
        let input = "A test, string";
        let output = "AXZATESTXZCXZASTRING";

        assert_eq!(output, encode_special_characters(input));
    }

    #[test]
    #[should_panic]
    fn test_should_panic_encode_bad_string() {
        let input = "A test, string@";
        encode_special_characters(input);
    }

    #[test]
    fn test_should_decode_good_string() {
        let input = "AXZATESTXZCXZASTRING";
        let output = "A TEST, STRING";

        assert_eq!(output, decode_special_characters(input));
    }

    #[test]
    fn test_generate_key_stream_null_key() {
        let mut deck = Deck::default();
        let key_stream: Vec<u8> = vec![4, 49, 10, 24, 8, 51, 44, 6, 4, 33, 20, 39, 19, 34, 42];
        let mut output: Vec<u8> = Vec::with_capacity(15);

        for _ in 0..15 {
            output.push(generate_key_stream(&mut deck));
        }

        assert_eq!(key_stream, output);
    }

    #[test]
    fn test_generate_key_stream_with_key() {
        let mut deck = Deck::from_key("F");
        let key_stream: Vec<u8> = vec![49, 24, 8, 46, 16, 1, 12, 33, 10, 10, 9, 27, 4, 32, 24];
        let mut output: Vec<u8> = Vec::with_capacity(15);

        for _ in 0..15 {
            output.push(generate_key_stream(&mut deck));
        }

        assert_eq!(key_stream, output);
    }

    #[test]
    fn test_pad_string() {
        let string = "HI";
        assert_eq!("HIZZZ", pad_string(string));
    }

    #[test]
    fn test_format_string() {
        let string = "I CANT LET YOU DO THAT DAVE";
        assert_eq!("ICANT LETYO UDOTH ATDAV E", format_string(string));
    }

    #[test]
    fn test_convert_plaintext_to_ciphertext_no_key() {
        let deck = Deck::default();
        let plaintext = "AAAAAAAAAAAAAAA";

        assert_eq!("EXKYIZSGEHUNTIQ", encrypt_plaintext(&plaintext, deck));
    }

    #[test]
    fn test_convert_plaintext_to_ciphertext_with_key() {
        let deck = Deck::from_key("cryptonomicon");
        let plaintext = "AAAAAAAAAAAAAAAAAAAAAAAAA";

        assert_eq!(
            "SUGSRSXSWQRMXOHIPBFPXARYQ",
            encrypt_plaintext(&plaintext, deck)
        );
    }

    #[test]
    fn test_convert_ciphertext_to_plaintext_no_key() {
        let deck = Deck::default();
        let ciphertext = "EXKYIZSGEHUNTIQ";

        assert_eq!("AAAAAAAAAAAAAAA", decrypt_ciphertext(&ciphertext, deck));
    }

    #[test]
    fn test_convert_ciphertext_to_plaintext_with_key() {
        let deck = Deck::from_key("CRYPTONOMICON");
        let ciphertext = "SUGSRSXSWQRMXOHIPBFPXARYQ";

        assert_eq!(
            "AAAAAAAAAAAAAAAAAAAAAAAAA",
            decrypt_ciphertext(&ciphertext, deck)
        );
    }
}
