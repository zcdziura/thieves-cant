/*
 * Copyright (C) 2018 Z. Charles Dziura
 *
 * This program is distributed under the terms of the GNU General Public
 * License. See the `COPYING` file for full terms.
 */

use std::convert::From;
use std::default::Default;
use std::fmt;

use rand::seq::SliceRandom;
use rand::thread_rng;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Deck {
    pub cards: Vec<Card>,
    pub joker_a_idx: usize,
    pub joker_b_idx: usize,
}

impl Deck {
    pub fn new() -> Self {
        let mut rng = thread_rng();
        let mut cards: Vec<Card> = (1..55).map(|value| Card::from(value)).collect();

        cards.shuffle(&mut rng);

        let joker_a_idx = find_joker('A', &cards);
        let joker_b_idx = find_joker('B', &cards);

        Deck {
            cards,
            joker_a_idx,
            joker_b_idx,
        }
    }

    pub fn from_key(key: &str) -> Self {
        let mut deck = Deck::default();
        let mut characters: Vec<char> = Vec::with_capacity(key.len());

        for c in key.chars() {
            if c.is_ascii_alphabetic() {
                characters.push(c.to_ascii_uppercase());
            } else {
                panic!("Error: key must only contain ASCII alphabetic characters.");
            }
        }

        for c in &characters {
            deck.move_joker_a();
            deck.move_joker_b();
            deck.triple_cut();
            deck.count_cut();
            deck.value_cut(*c);
        }

        deck
    }

    pub fn move_joker_a(&mut self) {
        if self.joker_a_idx == self.cards.len() - 1 {
            let card = self.cards.pop().unwrap();
            self.cards.insert(1, card);
        } else {
            self.cards.swap(self.joker_a_idx, self.joker_a_idx + 1);
        }

        self.joker_a_idx = find_joker('A', &self.cards);
        self.joker_b_idx = find_joker('B', &self.cards);
    }

    pub fn move_joker_b(&mut self) {
        if self.joker_b_idx == self.cards.len() - 2 {
            let idx = self.cards.len() - 2;
            let card = self.cards.remove(idx);
            self.cards.insert(1, card);
        } else if self.joker_b_idx == self.cards.len() - 1 {
            let card = self.cards.pop().unwrap();
            self.cards.insert(2, card);
        } else {
            let card = self.cards.remove(self.joker_b_idx);
            self.cards.insert(self.joker_b_idx + 2, card);
        }

        self.joker_a_idx = find_joker('A', &self.cards);
        self.joker_b_idx = find_joker('B', &self.cards);
    }

    pub fn triple_cut(&mut self) {
        let first_joker_idx = if self.joker_a_idx < self.joker_b_idx {
            self.joker_a_idx
        } else {
            self.joker_b_idx
        };

        let second_joker_idx = if first_joker_idx == self.joker_a_idx {
            self.joker_b_idx
        } else {
            self.joker_a_idx
        };

        let mut cards: Vec<Card> = Vec::with_capacity(54);
        if second_joker_idx < self.cards.len() - 1 {
            cards.extend(self.cards[second_joker_idx + 1..].iter().cloned());
        }
        cards.extend(
            self.cards[first_joker_idx..second_joker_idx + 1]
                .iter()
                .cloned(),
        );
        cards.extend(self.cards[..first_joker_idx].iter().cloned());

        let joker_a_idx = find_joker('A', &cards);
        let joker_b_idx = find_joker('B', &cards);

        *self = Deck {
            cards,
            joker_a_idx,
            joker_b_idx,
        };
    }

    pub fn count_cut(&mut self) {
        let mut cards = self.cards.clone();
        let last_card = cards.pop().unwrap();
        let last_card_value: usize = last_card.clone().into();

        let mut cut_deck: Vec<Card> = cards.drain(..last_card_value).collect();
        cards.append(&mut cut_deck);
        cards.push(last_card);

        let joker_a_idx = find_joker('A', &cards);
        let joker_b_idx = find_joker('B', &cards);

        *self = Deck {
            cards,
            joker_a_idx,
            joker_b_idx,
        };
    }

    pub fn value_cut(&mut self, value: char) {
        let mut cards = self.cards.clone();
        let last_card = cards.pop().unwrap();

        let mut cut_deck: Vec<Card> = cards.drain(..(value as usize) - 64).collect();
        cards.append(&mut cut_deck);
        cards.push(last_card);

        let joker_a_idx = find_joker('A', &cards);
        let joker_b_idx = find_joker('B', &cards);

        *self = Deck {
            cards,
            joker_a_idx,
            joker_b_idx,
        };
    }

    pub fn get_output_value(&self) -> Result<u8, ()> {
        let count_down_value: usize = match self.cards[0].clone().into() {
            53 | 54 => 53,
            v => v,
        };
        let output_card_value: usize = self.cards[count_down_value].clone().into();

        if output_card_value > 52 {
            Err(())
        } else {
            Ok(output_card_value as u8)
        }
    }
}

impl Default for Deck {
    fn default() -> Self {
        let mut cards: Vec<Card> = Vec::with_capacity(54);

        for idx in 1..55 {
            cards.push(Card::from(idx));
        }

        Deck {
            cards,
            joker_a_idx: 52,
            joker_b_idx: 53,
        }
    }
}

impl fmt::Display for Deck {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut display = String::new();
        for (idx, card) in self.cards.iter().enumerate() {
            if idx == self.cards.len() - 1 {
                display.push_str(&format!("{}", *card));
            } else {
                display.push_str(&format!("{}\n", *card));
            }
        }

        write!(f, "{}", display)
    }
}

impl<'a> From<&'a [usize]> for Deck {
    fn from(values: &[usize]) -> Deck {
        let cards: Vec<Card> = values.iter().map(|value| Card::from(*value)).collect();

        let joker_a_idx = find_joker('A', &cards);
        let joker_b_idx = find_joker('B', &cards);

        Deck {
            cards,
            joker_a_idx,
            joker_b_idx,
        }
    }
}

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Card {
    suit: Option<CardSuit>,
    value: CardValue,
}

impl Card {
    fn joker_a() -> Self {
        Card::from(53)
    }

    fn joker_b() -> Self {
        Card::from(54)
    }
}

impl fmt::Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let suit = self
            .suit
            .as_ref()
            .map_or("Joker".to_owned(), |suit| format!("{:?}", suit));

        if suit == "Joker" {
            write!(f, "{}", self.value)
        } else {
            write!(f, "{} of {}", self.value, suit)
        }
    }
}

impl From<usize> for Card {
    fn from(value: usize) -> Card {
        if value < 1 || value > 54 {
            panic!("Invalid card value: {}", value);
        }

        let (suit, value) = if value > 52 {
            (None, value)
        } else if value / 40 > 0 {
            (Some(CardSuit::Spades), value - 39)
        } else if value / 27 > 0 {
            (Some(CardSuit::Hearts), value - 26)
        } else if value / 14 > 0 {
            (Some(CardSuit::Diamonds), value - 13)
        } else {
            (Some(CardSuit::Clubs), value)
        };

        Card {
            suit,
            value: value.into(),
        }
    }
}

impl From<Card> for usize {
    fn from(value: Card) -> usize {
        if let Some(suit) = value.suit {
            let val: usize = value.value.into();
            let addend = match suit {
                CardSuit::Clubs => 0,
                CardSuit::Diamonds => 13,
                CardSuit::Hearts => 26,
                CardSuit::Spades => 39,
            };

            val + addend
        } else {
            match value.value {
                CardValue::JokerA => 53,
                CardValue::JokerB => 54,
                _ => unreachable!("Invalid card value: {:?}", value.value),
            }
        }
    }
}

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum CardSuit {
    Clubs,
    Diamonds,
    Hearts,
    Spades,
}

impl From<usize> for CardSuit {
    fn from(value: usize) -> CardSuit {
        match value {
            0 => CardSuit::Clubs,
            13 => CardSuit::Diamonds,
            26 => CardSuit::Hearts,
            39 => CardSuit::Spades,
            _ => panic!("Invalid suit value; must be 0, 13, 26, or 39: {}", value),
        }
    }
}

impl From<CardSuit> for usize {
    fn from(value: CardSuit) -> usize {
        match value {
            CardSuit::Clubs => 0,
            CardSuit::Diamonds => 13,
            CardSuit::Hearts => 26,
            CardSuit::Spades => 39,
        }
    }
}

fn find_joker(joker: char, cards: &[Card]) -> usize {
    if joker == 'A' {
        cards
            .iter()
            .position(|card| *card == Card::joker_a())
            .unwrap()
    } else {
        cards
            .iter()
            .position(|card| *card == Card::joker_b())
            .unwrap()
    }
}

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum CardValue {
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    JokerA,
    JokerB,
}

impl fmt::Display for CardValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let display = match self {
            &CardValue::JokerA => "Joker A".to_owned(),
            &CardValue::JokerB => "Joker B".to_owned(),
            _ => format!("{:?}", self),
        };

        write!(f, "{}", display)
    }
}

impl From<usize> for CardValue {
    fn from(value: usize) -> CardValue {
        match value {
            1 => CardValue::Ace,
            2 => CardValue::Two,
            3 => CardValue::Three,
            4 => CardValue::Four,
            5 => CardValue::Five,
            6 => CardValue::Six,
            7 => CardValue::Seven,
            8 => CardValue::Eight,
            9 => CardValue::Nine,
            10 => CardValue::Ten,
            11 => CardValue::Jack,
            12 => CardValue::Queen,
            13 => CardValue::King,
            53 => CardValue::JokerA,
            54 => CardValue::JokerB,
            _ => panic!("Invalid card value: {}", value),
        }
    }
}

impl From<CardValue> for usize {
    fn from(value: CardValue) -> usize {
        match value {
            CardValue::Ace => 1,
            CardValue::Two => 2,
            CardValue::Three => 3,
            CardValue::Four => 4,
            CardValue::Five => 5,
            CardValue::Six => 6,
            CardValue::Seven => 7,
            CardValue::Eight => 8,
            CardValue::Nine => 9,
            CardValue::Ten => 10,
            CardValue::Jack => 11,
            CardValue::Queen => 12,
            CardValue::King => 13,
            CardValue::JokerA => 53,
            CardValue::JokerB => 54,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Card, CardSuit, CardValue, Deck};
    use std::panic::catch_unwind;

    #[test]
    fn test_create_king_of_clubs() {
        let card = Card::from(13);
        let is_clubs = match card.suit {
            Some(CardSuit::Clubs) => true,
            _ => false,
        };

        let is_king = match card.value {
            CardValue::King => true,
            _ => false,
        };

        assert!(is_clubs && is_king);
    }

    #[test]
    fn test_create_jack_of_diamonds() {
        let card = Card::from(24);
        let is_diamonds = match card.suit {
            Some(CardSuit::Diamonds) => true,
            _ => false,
        };

        let is_jack = match card.value {
            CardValue::Jack => true,
            _ => false,
        };

        assert!(is_diamonds && is_jack);
    }

    #[test]
    fn test_create_queen_of_hearts() {
        let card = Card::from(38);
        let is_hearts = match card.suit {
            Some(CardSuit::Hearts) => true,
            _ => false,
        };

        let is_queen = match card.value {
            CardValue::Queen => true,
            _ => false,
        };

        assert!(is_hearts && is_queen);
    }

    #[test]
    fn test_create_ace_of_spades() {
        let card = Card::from(40);
        let is_spades = match card.suit {
            Some(CardSuit::Spades) => true,
            _ => false,
        };

        let is_ace = match card.value {
            CardValue::Ace => true,
            _ => false,
        };

        assert!(is_spades && is_ace);
    }

    #[test]
    fn test_create_joker() {
        let card = Card::joker_a();
        let is_none = match card.suit {
            None => true,
            _ => false,
        };

        let is_joker = match card.value {
            CardValue::JokerA => true,
            _ => false,
        };

        assert!(is_none && is_joker);
    }

    #[test]
    fn test_create_deck_from_key() {
        let deck1 = Deck::from_key("A");
        let deck2 = Deck::from(
            vec![
                3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45,
                46, 47, 48, 49, 50, 51, 52, 53, 54, 2, 1,
            ].as_slice(),
        );

        assert_eq!(deck1, deck2);
    }

    #[test]
    fn test_should_panic_create_bad_card() {
        let res1 = catch_unwind(|| Card::from(0));
        let res2 = catch_unwind(|| Card::from(55));

        assert!(res1.is_err());
        assert!(res2.is_err());
    }

    #[test]
    fn test_convert_card_to_number() {
        let card = Card::from(42);

        assert_eq!(42 as usize, card.into());
    }

    #[test]
    fn test_should_move_joker_a_down_one() {
        let mut deck1 = Deck::from(vec![1, 2, 53, 4, 54].as_slice());
        let deck2 = Deck::from(vec![1, 2, 4, 53, 54].as_slice());

        deck1.move_joker_a();

        assert_eq!(deck1, deck2);
        assert_eq!(deck1.joker_a_idx, 3);
    }

    #[test]
    fn test_should_move_joker_a_down_one_from_bottom() {
        let mut deck1 = Deck::from(vec![1, 2, 3, 54, 53].as_slice());
        let deck2 = Deck::from(vec![1, 53, 2, 3, 54].as_slice());

        deck1.move_joker_a();

        assert_eq!(deck1, deck2);
        assert_eq!(deck1.joker_a_idx, 1)
    }

    #[test]
    fn test_should_move_joker_b_down_two() {
        let mut deck1 = Deck::from(vec![1, 54, 3, 4, 53].as_slice());
        let deck2 = Deck::from(vec![1, 3, 4, 54, 53].as_slice());

        deck1.move_joker_b();

        assert_eq!(deck1, deck2);
        assert_eq!(deck1.joker_b_idx, 3);
    }

    #[test]
    fn test_should_move_joker_b_down_second_from_bottom() {
        let mut deck1 = Deck::from(vec![1, 2, 53, 54, 4].as_slice());
        let deck2 = Deck::from(vec![1, 54, 2, 53, 4].as_slice());

        deck1.move_joker_b();

        assert_eq!(deck1, deck2);
        assert_eq!(deck1.joker_b_idx, 1);
    }

    #[test]
    fn test_should_move_joker_b_down_last_card() {
        let mut deck1 = Deck::from(vec![1, 2, 3, 53, 54].as_slice());
        let deck2 = Deck::from(vec![1, 2, 54, 3, 53].as_slice());

        deck1.move_joker_b();

        assert_eq!(deck1, deck2);
        assert_eq!(deck1.joker_b_idx, 2);
    }

    #[test]
    fn test_should_do_triple_cut() {
        let mut deck1 =
            Deck::from(vec![1, 2, 3, 53, 4, 5, 6, 7, 8, 9, 10, 54, 11, 12, 13].as_slice());
        let deck2 = Deck::from(vec![11, 12, 13, 53, 4, 5, 6, 7, 8, 9, 10, 54, 1, 2, 3].as_slice());

        deck1.triple_cut();

        assert_eq!(deck1.cards.len(), deck2.cards.len());
        assert_eq!(deck1, deck2);
    }

    #[test]
    fn test_should_do_count_cut() {
        let mut deck1 =
            Deck::from(vec![1, 2, 3, 53, 4, 5, 6, 7, 8, 9, 10, 54, 11, 12, 13].as_slice());
        let deck2 = Deck::from(vec![12, 1, 2, 3, 53, 4, 5, 6, 7, 8, 9, 10, 54, 11, 13].as_slice());

        deck1.count_cut();

        assert_eq!(deck1.cards.len(), deck2.cards.len());
        assert_eq!(deck1, deck2);
    }

    #[test]
    fn test_should_do_value_cut() {
        let mut deck1 =
            Deck::from(vec![1, 2, 3, 53, 4, 5, 6, 7, 8, 9, 10, 54, 11, 12, 13].as_slice());
        let deck2 = Deck::from(vec![2, 3, 53, 4, 5, 6, 7, 8, 9, 10, 54, 11, 12, 1, 13].as_slice());

        deck1.value_cut('A');

        assert_eq!(deck1.cards.len(), deck2.cards.len());
        assert_eq!(deck1, deck2);
    }

    #[test]
    fn test_get_output_for_two_of_clubs() {
        let deck = Deck::default();
        let output = deck.get_output_value();

        assert!(output.is_ok());
        assert_eq!(2, output.unwrap());
    }

    #[test]
    fn test_get_output_for_joker() {
        let deck = Deck::from(vec![3, 2, 1, 53, 4, 5, 6, 7, 8, 9, 10, 54, 11, 12, 13].as_slice());
        let output = deck.get_output_value();

        assert!(output.is_err());
    }
}
