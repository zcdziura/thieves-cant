/*
 * Thieves' Cant - A Rust implementation of the Solitaire Cipher designed by Bruce Schneier.
 * Copyright (C) 2018 Z. Charles Dziura
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#[macro_use]
extern crate clap;
#[macro_use]
extern crate lazy_static;
extern crate rand;
extern crate regex;

mod deck;
mod pontifex;

use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, BufWriter, ErrorKind};
use std::process;

use crate::deck::Deck;
use crate::pontifex::{
    decode_special_characters, decrypt_ciphertext, encode_special_characters, encrypt_plaintext, format_string, pad_string,
};
use regex::Regex;

fn main() {
    let mut app = clap_app!(tvct =>
        (version: env!("CARGO_PKG_VERSION"))
        (about: "A Rust implementation of the Solitaire Cipher designed by Bruce Schneier.")
        (@subcommand deck =>
            (about: "Commands for creating and displaying the deck of cards; by default will print out the deck")
            (@arg NEW: -n --new "Creates a new keyed deck; defaults to filename 'thieves-cant.deck'")
            (@arg KEY: -k --key +takes_value "The key used to to set the order of the deck; defaults to null")
            (@arg FILE: -f --file +takes_value "Specify the file location of the deck; defaults to 'thieves-cant.deck'")
        )
        (@subcommand encrypt =>
            (about: "Encrypt a plaintext message")
            (@arg PLAINTEXT: +required "The plaintext to be encrypted")
            (@arg FILE: -f --file +takes_value "Specify the file location of the deck; defaults to 'thieves-cant.deck'")
        )
        (@subcommand decrypt =>
            (about: "Decrypt a ciphered message")
            (@arg CIPHERTEXT: +required "The ciphertext to be decrypted")
            (@arg FILE: -f --file +takes_value "Specify the file location of the deck; defaults to 'thieves-cant.deck'")
        )
    );

    let matches = app.clone().get_matches();
    let filename = matches.value_of("FILE").unwrap_or("thieves-cant.deck");

    if let Some(matches) = matches.subcommand_matches("deck") {
        let key = matches.value_of("KEY");

        if matches.is_present("NEW") {
            new_deck(filename, key);
        } else {
            match load_deck(filename) {
                Ok(deck) => println!("{}", deck),
                Err(e) => {
                    eprintln!("{}", e);
                    process::exit(1);
                }
            }
        }
    } else if let Some(matches) = matches.subcommand_matches("encrypt") {
        if let Some(plaintext) = matches.value_of("PLAINTEXT") {
            let deck = match load_deck(filename) {
                Ok(d) => d,
                Err(e) => {
                    eprintln!("{}", e);
                    process::exit(1);
                }
            };

            encrypt(plaintext, deck);
        } else {
            eprintln!("Thieves Cant Error: Please enter a message to encrypt");
            process::exit(1);
        }
    } else if let Some(matches) = matches.subcommand_matches("decrypt") {
        if let Some(ciphertext) = matches.value_of("CIPHERTEXT") {
            let deck = match load_deck(filename) {
                Ok(d) => d,
                Err(e) => {
                    eprintln!("{}", e);
                    process::exit(1);
                }
            };

            decrypt(ciphertext, deck);
        } else {
            eprintln!("Thieves Cant Error: Please enter a message to decrypt");
            process::exit(1);
        }
    } else {
        let _ = app.print_help();
    }
}

fn new_deck(filename: &str, key: Option<&str>) {
    let mut file = BufWriter::new(File::create(filename).unwrap());
    let deck = match key {
        Some(key) => Deck::from_key(key),
        None => Deck::new(),
    };

    let cards: String = deck
        .cards
        .iter()
        .enumerate()
        .map(|val| {
            let (idx, card) = val;
            let value: usize = card.clone().into();
            if idx == deck.cards.len() - 1 {
                value.to_string()
            } else {
                format!("{} ", value.to_string())
            }
        }).collect();

    match file.write(cards.as_bytes()) {
        Ok(_) => println!("Successfully created new keyed deck"),
        Err(e) => panic!("Thieves-Cant Error: {}", e),
    }
}

fn load_deck(filename: &str) -> Result<Deck, String> {
    let file = match File::open(filename) {
        Ok(f) => f,
        Err(e) => match e.kind() {
            ErrorKind::NotFound => {
                return Err(format!(
                    "Thieves-Cant Error: No deck file found: {}",
                    filename
                ))
            }
            ErrorKind::PermissionDenied => {
                return Err(format!(
                    "Thieves-Cant Error: Permission denied; cannot open deck file: {}",
                    filename
                ))
            }
            _ => {
                return Err(format!(
                    "Thieves-Cant Error: Cannot open deck file: {}",
                    filename
                ))
            }
        },
    };
    let mut reader = BufReader::new(file);
    let mut contents = String::new();

    let _ = reader.read_to_string(&mut contents);

    let cards: Vec<usize> = contents
        .split_whitespace()
        .map(|value| value.parse::<usize>().unwrap())
        .collect();

    Ok(Deck::from(cards.as_slice()))
}

fn encrypt(plaintext: &str, deck: Deck) {
    let encoded_text = pad_string(&encode_special_characters(plaintext));
    let ciphertext = format_string(&encrypt_plaintext(&encoded_text, deck));

    println!("{}", ciphertext);
}

fn decrypt(ciphertext: &str, deck: Deck) {
    lazy_static! {
        static ref RE: Regex = Regex::new("([Z]+)$").unwrap();
    }

    let plaintext = decrypt_ciphertext(ciphertext, deck);
    let stripped_text = RE.replace_all(&plaintext, "");

    println!("{}", decode_special_characters(&stripped_text));
}
